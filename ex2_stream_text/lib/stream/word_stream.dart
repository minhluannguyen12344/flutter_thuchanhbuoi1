import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:math';

class WordStream {
  int counter = 0;
  List<String> wordList = [];

  fetchWords() {
    counter = 0;
    Uri url =
        Uri.parse("https://random-word-api.herokuapp.com/word?number=100");
    http.get(url).then((result) {
      var jsonObj = json.decode(result.body);
      wordList = List<String>.from(jsonObj.map((w) => w));
    });
  }

  WordStream() {
    fetchWords();
  }

  Stream<String?> getWord() async* {
    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      final random = Random();
      // generate a random index based on the list length
      // and use it to retrieve the element
      return wordList[random.nextInt(wordList.length)];
    });
  }

  Stream<int?> countWord() async* {
    yield* Stream.periodic(Duration(seconds: 1), (int t) {
      return counter++;
    });
  }
}
