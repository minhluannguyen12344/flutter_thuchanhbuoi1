import 'dart:html';
import 'dart:math';

import 'package:clone_zalo/models/user_chat.dart';
import 'package:clone_zalo/models/user.dart';
import 'package:flutter/material.dart';
import 'package:clone_zalo/screens/sreach_page.dart';
import "./user_chat.dart";

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        // add tabBarTheme
        tabBarTheme: const TabBarTheme(
            labelColor: Colors.black,
            labelStyle: TextStyle(color: Colors.white), // color for text
            indicator: UnderlineTabIndicator(
                borderSide: BorderSide(color: Colors.white))),
        primaryColor: Colors.grey,
      ),
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            leading: Builder(
              builder: (BuildContext context) {
                return Row(
                  children: [
                    IconButton(
                      icon: Icon(Icons.search),
                      onPressed: () => Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => search_page())),
                    ),
                  ],
                );
              },
            ),
            title: TextButton(
              child: Text(
                'Tìm bạn bè , tin nhắn...',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => search_page())),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.qr_code),
              ),
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.add),
              ),
            ],
          ),
          body: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                  color: Colors.black,
                  width: 0.3,
                ))),
                child: TabBar(
                  tabs: [
                    Tab(
                      child: Text(
                        'Cập nhật thông tin cá nhân',
                        style: TextStyle(color: Colors.black),
                      ),
                      height: 20,
                    ),
                  ],
                ),
              ),
              Expanded(
                child: SizedBox(
                  child: SingleChildScrollView(
                    physics: ScrollPhysics(),
                    child: Column(
                      children: <Widget>[
                        ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: chats.length,
                          itemBuilder: (BuildContext context, int index) {
                            final Message chat = chats[index];

                            return Container(
                              child: ListTile(
                                onTap: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) =>
                                        chatscreen(user: chat.sender),
                                  ),
                                ),
                                title: Text(chat.sender.name),
                                leading: CircleAvatar(
                                  radius: 30,
                                  backgroundImage: AssetImage(
                                    chat.sender.imageURL,
                                  ),
                                ),
                                trailing: Text(chat.time),
                                subtitle: Container(
                                  margin: EdgeInsets.fromLTRB(0, 5, 0, 3),
                                  padding: EdgeInsets.only(bottom: 5),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                        width: 0.2,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                    chat.text,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            alignment: Alignment.center,
                            child: TextButton(
                              onPressed: () {},
                              child: Text(
                                'xem thêm',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.blue,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.grey.shade300,
                          width: double.infinity,
                          height: 100,
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.all(20.0),
                                child: Text(
                                  'Dễ dàng tìm kiếm và trò chuyện với bạn bè',
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ),
                              Container(
                                width: 200,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    color: Colors.blue,
                                    border: Border()),
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'tìm thêm bạn',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
