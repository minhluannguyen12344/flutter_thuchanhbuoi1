import 'package:flutter/material.dart';
import 'package:clone_zalo/models/user.dart';
import 'package:clone_zalo/models/user_chat.dart';

class chatscreen extends StatelessWidget {
  final User user;
  const chatscreen({required this.user});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name),
      ),
    );
  }
}
