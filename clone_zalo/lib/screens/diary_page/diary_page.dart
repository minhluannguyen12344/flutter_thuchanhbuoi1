import 'dart:html';
import 'package:flutter/material.dart';
import 'package:clone_zalo/screens/sreach_page.dart';

class diary_page extends StatefulWidget {
  const diary_page({Key? key}) : super(key: key);

  @override
  State<diary_page> createState() => _diary_pageState();
}

class _diary_pageState extends State<diary_page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(Icons.search),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => search_page())),
            );
          },
        ),
        title: TextButton(
          child: Text(
            'Tìm bạn bè , tin nhắn...',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => search_page())),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.post_add),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.notifications),
          ),
        ],
      ),
      body: Text("diary"),
    );
  }
}
