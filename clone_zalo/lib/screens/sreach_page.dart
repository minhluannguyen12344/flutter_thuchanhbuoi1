import 'package:flutter/material.dart';

class search_page extends StatelessWidget {
  const search_page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: double.infinity,
          height: 40,
          padding: EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: TextField(
            decoration: InputDecoration(
              hintText: "Tìm bạn bè,tin nhắn...",
              border: InputBorder.none,
            ),
          ),
        ),
        actions: [Icon(Icons.qr_code), Padding(padding: EdgeInsets.all(10))],
      ),
    );
  }
}
