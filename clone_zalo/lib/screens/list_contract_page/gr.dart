import 'package:flutter/material.dart';
import 'package:clone_zalo/models/group_chat.dart';

class gr extends StatefulWidget {
  const gr({Key? key}) : super(key: key);

  @override
  State<gr> createState() => _grState();
}

class _grState extends State<gr> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: <Widget>[
          Container(
            child: (ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(2.0),
              children: [
                ListTile(
                  onTap: () {},
                  title: Text(
                    "Tạo nhóm mới",
                    style: TextStyle(color: Colors.blue),
                  ),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/avt10.jpg'),
                  ),
                ),
              ],
            )),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Text('Nhóm đang tham gia (2)'),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: chats.length,
            itemBuilder: (BuildContext context, int index) {
              final Message grchat = chats[index];
              Padding(
                padding: EdgeInsets.all(15.0),
              );
              return Container(
                child: ListTile(
                  onTap: () {},
                  title: Text(grchat.sender.name),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage(grchat.sender.imageURL),
                  ),
                  trailing: Text(grchat.time),
                  subtitle: Container(
                    padding: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          width: 0.2,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    child: Text(
                      grchat.text,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
