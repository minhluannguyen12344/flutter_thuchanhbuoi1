import 'package:flutter/material.dart';

class oa extends StatefulWidget {
  const oa({Key? key}) : super(key: key);

  @override
  State<oa> createState() => _oaState();
}

class _oaState extends State<oa> {
  final title = ["Báo mới", "Ví QR", "Zalo Pay"];
  final avt_img = [
    "assets/images/avt14.jpg",
    "assets/images/avt15.jpg",
    "assets/images/avt4.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: <Widget>[
          Container(
            child: (ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(2.0),
              children: [
                ListTile(
                  onTap: () {},
                  title: Text(
                    "Tìm thêm Official Account",
                    style: TextStyle(color: Colors.blue),
                  ),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/avt13.jpg'),
                  ),
                ),
              ],
            )),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(color: Colors.white),
            child: Text('official Account đang quan tâm'),
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: title.length,
            itemBuilder: (context, index) {
              return Container(
                child: ListTile(
                  onTap: () {},
                  title: Text(title[index]),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage(avt_img[index]),
                  ),
                ),
                padding: EdgeInsets.all(15),
              );
            },
          )
        ],
      ),
    );
  }
}
