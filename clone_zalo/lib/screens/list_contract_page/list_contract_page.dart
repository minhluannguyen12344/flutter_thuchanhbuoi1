import 'dart:html';
import 'package:flutter/material.dart';
import './friend.dart';
import './gr.dart';
import './oa.dart';
import 'package:clone_zalo/screens/sreach_page.dart';

class list_contract_page extends StatefulWidget {
  const list_contract_page({Key? key}) : super(key: key);

  @override
  State<list_contract_page> createState() => _list_contract_pageState();
}

class _list_contract_pageState extends State<list_contract_page> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => search_page())),
                );
              },
            ),
            title: TextButton(
              child: Text(
                'Tìm bạn bè , tin nhắn...',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => search_page())),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () {},
                icon: Icon(Icons.person_add_alt),
              ),
            ],
          ),
          body: Column(
            children: [
              TabBar(
                indicatorColor: Colors.grey,
                tabs: <Tab>[
                  Tab(
                    child: Text(
                      'Bạn bè',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Nhóm',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Tab(
                    child: Text(
                      'Oa',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: SizedBox(
                  child: TabBarView(
                    children: [friend(), gr(), oa()],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
