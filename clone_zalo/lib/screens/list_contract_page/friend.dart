import 'package:flutter/material.dart';
import 'package:clone_zalo/models/user_chat.dart';
import 'package:clone_zalo/models/user.dart';

class friend extends StatefulWidget {
  const friend({Key? key}) : super(key: key);

  @override
  State<friend> createState() => _friendState();
}

class _friendState extends State<friend> {
  final titles = ["Hào", "Vũ", "Tùng"];
  final sort_with_name = ["H", "V", "T"];
  final avt_img = [
    'assets/images/avt7.jpg',
    'assets/images/avt9.jpg',
    'assets/images/avt8.jpg'
  ];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: <Widget>[
          Container(
            child: (ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(2.0),
              children: [
                ListTile(
                  onTap: () {},
                  title: Text("Lời mời kết bạn"),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/avt6.jpg'),
                  ),
                ),
                ListTile(
                  onTap: () {},
                  title: Text("Danh bạ máy"),
                  subtitle: Text('Các liên hệ có dùng zalo'),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/avt5.jpg'),
                  ),
                ),
              ],
            )),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 0.2, color: Colors.black),
              ),
            ),
            padding: EdgeInsets.all(8.0),
            child: Row(
              children: [
                TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.grey.shade300)),
                  onPressed: () {},
                  child: Text('Tất cả 11'),
                ),
                Padding(padding: EdgeInsets.all(8.0)),
                TextButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          side: BorderSide(color: Colors.grey, width: 0.2),
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.white70)),
                  onPressed: () {},
                  child: Text('Mới truy cập 0'),
                ),
              ],
            ),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: titles.length,
            itemBuilder: (context, index) {
              return Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        margin: EdgeInsets.fromLTRB(20, 10, 0, 8),
                        child: Text(sort_with_name[index])),
                    ListTile(
                      onTap: () {
                        final check = titles[index];
                        print("you have click to your friend $check");
                      },
                      title: Text(titles[index]),
                      leading: CircleAvatar(
                        backgroundImage: AssetImage(avt_img[index]),
                      ),
                      trailing: Wrap(
                        spacing: 10,
                        children: [
                          Icon(Icons.phone),
                          Icon(Icons.video_call),
                        ],
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
