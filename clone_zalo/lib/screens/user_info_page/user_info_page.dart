import 'dart:html';
import 'package:flutter/material.dart';
import 'package:clone_zalo/screens/sreach_page.dart';

class user_page extends StatefulWidget {
  const user_page({Key? key}) : super(key: key);

  @override
  State<user_page> createState() => _user_pageState();
}

class _user_pageState extends State<user_page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(Icons.search),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => search_page())),
            );
          },
        ),
        title: TextButton(
          child: Text(
            'Tìm bạn bè , tin nhắn...',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => search_page())),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.settings),
          ),
        ],
      ),
      body: Column(
        children: [
          Container(
            child: ListTile(
                onTap: () {},
                leading: CircleAvatar(
                  backgroundColor: Colors.grey,
                  backgroundImage: NetworkImage(
                      "https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png"),
                ),
                title: Text("Nguyễn Minh Luân"),
                subtitle: Text('Xem trang cá nhân'),
                trailing: IconButton(
                  icon: Icon(
                    Icons.manage_accounts,
                    color: Colors.blue,
                  ),
                  onPressed: () {},
                )),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: ListTile(
              onTap: () {},
              leading: Icon(
                Icons.account_balance_wallet_outlined,
                color: Colors.blue,
              ),
              title: Text("Ví QR"),
              subtitle: Text('Lưu trữ và xuất trình các mã QR quan trọng'),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            child: ListTile(
              onTap: () {},
              leading: Icon(
                Icons.cloud,
                color: Colors.blue,
              ),
              title: Text("Clound của tôi"),
              subtitle: Text('Lưu trữ các tin nhắn quan trọng'),
            ),
          ),
          Container(
            decoration: BoxDecoration(color: Colors.grey.shade300),
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: ListTile(
              onTap: () {},
              leading: Icon(
                Icons.verified_user_outlined,
                color: Colors.blue,
              ),
              title: Text("Tài khoản và bảo mật"),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: ListTile(
              onTap: () {},
              leading: Icon(
                Icons.lock_outlined,
                color: Colors.blue,
              ),
              title: Text("Quyền riêng tư"),
            ),
          ),
        ],
      ),
    );
  }
}
