import 'dart:html';
import 'package:flutter/material.dart';
import 'package:clone_zalo/screens/sreach_page.dart';

class explore_page extends StatefulWidget {
  const explore_page({Key? key}) : super(key: key);

  @override
  State<explore_page> createState() => _explore_pageState();
}

class _explore_pageState extends State<explore_page> {
  final label_name = ["Gần bạn", "Thực phẩm", "Đồ ăn vặt", "Đặc sản"];
  final avt_img = [
    "assets/images/avt16.jpg",
    "assets/images/avt17.jpg",
    "assets/images/avt18.jpg",
    "assets/images/avt19.jpg",
  ];
  final label_name_tienich = [
    "Shop",
    "Sticker",
    "eGorvernment",
    "Fiza",
    "Ví ZaloPay",
    "Nạp tiền ĐT",
    "Trả Hóa Đơn",
    "Shop Lazada",
    "Home & car"
  ];
  final avt_img_tienich = [
    "assets/images/avt20.jpg",
    "assets/images/avt21.jpg",
    "assets/images/avt22.jpg",
    "assets/images/avt23.jpg",
    "assets/images/avt4.jpg",
    "assets/images/avt24.jpg",
    "assets/images/avt25.jpg",
    "assets/images/avt26.jpg",
    "assets/images/avt27.jpg",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: Icon(Icons.search),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => search_page())),
            );
          },
        ),
        title: TextButton(
          child: Text(
            'Tìm bạn bè , tin nhắn...',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => search_page())),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.qr_code),
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
              child: (ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.all(2.0),
                children: [
                  ListTile(
                    onTap: () {},
                    title: Text(
                      "Zalo Connect",
                      style: TextStyle(color: Colors.black, fontSize: 20),
                    ),
                    subtitle: Text("Đồ ngon vài bước chân"),
                    trailing: Icon(Icons.navigate_next),
                  ),
                ],
              )),
            ),
            Container(
              margin: EdgeInsets.all(20),
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                image: DecorationImage(
                    image: AssetImage("assets/images/map.jpg"),
                    fit: BoxFit.cover),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Padding(padding: EdgeInsets.all(16)),
                  GestureDetector(
                    onTap: () {
                      print("you press search button");
                    },
                    child: Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(8.0),
                      padding: EdgeInsets.all(4.0),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Icon(
                        Icons.search,
                      ),
                    ),
                  ),
                  Container(
                    child: GridView.builder(
                      primary: false,
                      reverse: false,
                      shrinkWrap: true,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                      ),
                      padding: EdgeInsets.all(10.0),
                      itemCount: label_name.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            print("${label_name[index]}");
                          },
                          child: Column(
                            children: [
                              CircleAvatar(
                                backgroundImage: AssetImage(avt_img[index]),
                                backgroundColor: Colors.white,
                              ),
                              Padding(padding: EdgeInsets.all(8.0)),
                              Text(label_name[index]),
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(color: Colors.grey.shade300),
              height: 10,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(22, 10, 20, 0),
              alignment: Alignment.topLeft,
              child: Text(
                'Tiện ích',
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              child: GridView.builder(
                primary: false,
                reverse: false,
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                ),
                padding: EdgeInsets.all(10.0),
                itemCount: label_name_tienich.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      print("${label_name_tienich[index]}");
                    },
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage: AssetImage(avt_img_tienich[index]),
                          backgroundColor: Colors.grey,
                        ),
                        Padding(padding: EdgeInsets.all(8.0)),
                        Text(label_name_tienich[index]),
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
