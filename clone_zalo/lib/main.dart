import 'package:flutter/material.dart';
import './screens/chatpage/chatpage.dart';
import 'screens/diary_page/diary_page.dart';
import './screens/list_contract_page/list_contract_page.dart';
import './screens/user_info_page/user_info_page.dart';
import './screens/explore_page/explore_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int currentIndex = 0;
  final Screens = [
    ChatPage(),
    list_contract_page(),
    explore_page(),
    diary_page(),
    user_page(),
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Screens[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.lightBlue,
          unselectedItemColor: Colors.grey.shade600,
          selectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          onTap: (index) => setState(() => currentIndex = index),
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.message),
              label: "tin nhắn",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_box),
              label: "Danh bạ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.widgets),
              label: "Khám phá",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.schedule),
              label: "Nhật kí",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Cá nhân",
            ),
          ],
        ),
      ),
    );
  }
}
