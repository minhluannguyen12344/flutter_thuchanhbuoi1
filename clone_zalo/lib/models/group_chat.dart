import 'package:clone_zalo/models/group.dart';

class Message {
  final Group sender;
  final String time;
  final String text;
  final bool unread;

  Message({
    required this.sender,
    required this.time,
    required this.text,
    required this.unread,
  });
}

List<Message> chats = [
  Message(
    sender: boi,
    time: '1/4',
    text: 'Luân:[Hình ảnh]',
    unread: true,
  ),
  Message(
    sender: cau_long,
    time: '29/3',
    text: 'Thi đã rời khỏi nhóm',
    unread: true,
  )
];
