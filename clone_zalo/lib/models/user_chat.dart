import 'package:clone_zalo/models/user.dart';

class Message {
  final User sender;
  final String time;
  final String text;
  final bool unread;

  Message({
    required this.sender,
    required this.time,
    required this.text,
    required this.unread,
  });
}

List<Message> chats = [
  Message(
    sender: MediaBox,
    time: 'T2',
    text: 'Zing:Một số đơn vị Nga đã rút khỏi Ukraie vì tổn thất nặng ',
    unread: true,
  ),
  Message(
    sender: ZaloPay,
    time: 'T2',
    text: 'Bạn đã nhận quà 1.000.000đ chưa ?',
    unread: true,
  ),
  Message(
    sender: nhomboi,
    time: 'T2',
    text:
        'Hân Phạm:Tuần này các bạn tiếp tục tập luyện nhóm 1 lên trường,nhóm 2 ở nhà quay video cô điểm danh trong ngày',
    unread: true,
  ),
  Message(
    sender: Zalo,
    time: '16/3',
    text: 'Zalo đăng nhập trên máy tính thành công',
    unread: true,
  ),
  Message(
    sender: boyte,
    time: '07/02',
    text: 'Bản tin bộ Y tế về tình hình chống dịch Việt Nam',
    unread: true,
  ),
  Message(
    sender: Tung,
    time: '04/10',
    text: 'eheheheeheheheheheheh',
    unread: true,
  ),
];
