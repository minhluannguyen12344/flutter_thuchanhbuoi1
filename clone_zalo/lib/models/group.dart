class Group {
  final int id;
  final String name;
  final String imageURL;
  final bool isonl;

  Group({
    required this.id,
    required this.name,
    required this.imageURL,
    required this.isonl,
  });
}

final Group boi = Group(
  id: 1,
  name: 'Nhóm bơi 100',
  imageURL: 'assets/images/avt11.jpg',
  isonl: true,
);

final Group cau_long = Group(
  id: 2,
  name: 'Nhóm cầu lông 15',
  imageURL: 'assets/images/avt12.jpg',
  isonl: true,
);
