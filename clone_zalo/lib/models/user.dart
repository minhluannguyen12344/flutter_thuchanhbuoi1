class User {
  final int id;
  final String name;
  final String imageURL;
  final bool isonl;

  User({
    required this.id,
    required this.name,
    required this.imageURL,
    required this.isonl,
  });
}

final User currentUser = User(
  id: 0,
  name: 'Luke',
  imageURL: 'assets/images/avt3.jpg',
  isonl: true,
);

final User MediaBox = User(
  id: 1,
  name: 'Media Box',
  imageURL: 'assets/images/avt2.jpg',
  isonl: true,
);

final User ZaloPay = User(
  id: 2,
  name: 'ZaloPay',
  imageURL: 'assets/images/avt4.jpg',
  isonl: true,
);

final User nhomboi = User(
  id: 3,
  name: 'Nhóm bơi 15',
  imageURL: 'assets/images/avt28.jpg',
  isonl: true,
);

final User Zalo = User(
  id: 4,
  name: 'Zalo',
  imageURL: 'assets/images/avt29.jpg',
  isonl: true,
);

final User boyte = User(
  id: 5,
  name: 'Bộ Y tế',
  imageURL: 'assets/images/avt30.jpg',
  isonl: true,
);

final User Tung = User(
  id: 6,
  name: 'Đinh Nguyễn Nhật Tùng',
  imageURL: 'assets/images/avt31.jpg',
  isonl: true,
);
