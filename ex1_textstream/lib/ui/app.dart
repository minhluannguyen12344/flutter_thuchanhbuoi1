import 'dart:html';

import 'package:flutter/material.dart';
import '../stream/color_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  ColorStream colorStream = ColorStream();
  List<String> textList = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Text',
      home: Scaffold(
        appBar: AppBar(
          title: Text('App Bar'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  itemCount: textList.length,
                  padding: const EdgeInsets.all(8),
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 50,
                      child: Center(
                        child: Text(textList[index]),
                      ),
                    );
                  }),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('create'),
          onPressed: () {
            changeColor();
          },
        ),
      ),
    );
  }

  changeColor() async {
    colorStream.getColors().listen((text) => setState(() {
          textList.add(text);
        }));
  }
}
