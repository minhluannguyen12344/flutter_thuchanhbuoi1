import 'package:flutter/material.dart';

class ColorStream {
  // Stream colorStream;

  Stream<String> getColors() async* {
    final String teststream = 'Hello';

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      return teststream;
    });
  }
}
