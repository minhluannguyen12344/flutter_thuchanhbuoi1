import 'dart:math';

import 'package:ex2_imgstream/models/img_model.dart';
import 'package:flutter/material.dart';
import '../stream/color_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget> {
  GetImageStream stream = GetImageStream();
  List<ImageModel> imgList = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Text',
      home: Scaffold(
        appBar: AppBar(
          title: Text('App Bar'),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                  itemCount: imgList.length,
                  padding: const EdgeInsets.all(8),
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 50,
                      width: 100,
                      child: Center(
                        child: Image.network(imgList[index].url ?? ""),
                      ),
                    );
                  }),
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Text('create'),
          onPressed: () {
            changeColor();
          },
        ),
      ),
    );
  }

  changeColor() async {
    stream.getImage().listen((imageObj) => setState(() {
          imgList.add(imageObj!);
        }));
  }
}
