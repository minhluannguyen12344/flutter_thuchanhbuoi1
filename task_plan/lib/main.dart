import 'package:flutter/material.dart';
import 'package:task_plan/plan_provider.dart';
import 'views/app.dart';
import 'plan_provider.dart';

void main() {
  var planProvider = PlanProvider(child: MyPlanApp());
  runApp(planProvider);
}
