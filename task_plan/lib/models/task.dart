import '../repositories/repository.dart';
import 'package:flutter/foundation.dart';

class Task {
  final id;
  String description;
  bool complete;
}

Task({
  required this.id,
  this.complete=false,
  this.description=''
});

Task.fromModel(Model model)
  : id = model.id,
        description = model.data['description'],
        complete = model.data['complete'];

Model toModel()=>
  Model(id:id,data:{'descripton':description,'complete':complete});


