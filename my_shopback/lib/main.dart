import 'package:flutter/material.dart';

import 'ui/account_screen/account_screen.dart';
import 'ui/band_screen/band_screen.dart';
import 'ui/like_screen/like_screen.dart';
import 'ui/main_screen/main_screen.dart';
import 'ui/prize _screen/prize_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int currentIndex = 0;
  final Screens = [
    Main_Screen(),
    Band_Screen(),
    Like_Screen(),
    Prize_Screen(),
    Account_Screen(),
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My ShopBack',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Screens[currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.lightBlue,
          unselectedItemColor: Colors.grey.shade600,
          selectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
          unselectedLabelStyle: TextStyle(fontWeight: FontWeight.w600),
          type: BottomNavigationBarType.fixed,
          currentIndex: currentIndex,
          onTap: (index) => setState(() => currentIndex = index),
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Trang chủ",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.store_outlined),
              label: "Thương Hiệu",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: "Yêu Thích",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.stars_outlined),
              label: "Tiền Thưởng",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Tài khoản",
            ),
          ],
        ),
      ),
    );
  }
}
