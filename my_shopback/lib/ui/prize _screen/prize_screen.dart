import 'package:flutter/material.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';

class Prize_Screen extends StatelessWidget {
  const Prize_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final thuthach = ['Đang diễn ra', 'Phần Thương', 'Lịch Sử'];
    final icon_thuthach = [
      Icons.directions_run_outlined,
      Icons.emoji_events_outlined,
      Icons.schedule_outlined
    ];
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: Colors.grey.shade300),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white70,
          title: const Text(
            'Thử Thách',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: const Center(
                          child: Text('Xem ngay phần thưởng của bạn'),
                        ),
                        content: TextField(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                            hintText: 'Nhập mã truy cập tại đây',
                          ),
                        ),
                        actions: [
                          GestureDetector(
                            onTap: () {
                              Navigator.pop(context, 'ok');
                            },
                            child: Container(
                              margin: EdgeInsets.all(20.0),
                              height: 50,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.red),
                              child: Center(child: Text('Sử dụng')),
                            ),
                          )
                        ],
                      );
                    });
              },
              child: Container(
                margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Row(
                    children: const [
                      Icon(
                        Icons.view_in_ar_outlined,
                        color: Colors.black,
                      ),
                      Text(
                        'Mã',
                        style: TextStyle(color: Colors.black),
                      )
                    ],
                  ),
                ),
              ),
            ),
            IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.info_outline,
                  color: Colors.black,
                ))
          ],
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(8.0),
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GridView.builder(
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                ),
                itemCount: thuthach.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.all(10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Icon(icon_thuthach[index]),
                        Text(thuthach[index]),
                        Text('0')
                      ],
                    ),
                  );
                },
              ),
              Container(
                margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                padding: EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Thử thách của bạn',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    ListTile(
                      leading: const Image(
                        image: AssetImage('assets/img/img1.PNG'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text('Tặng 100.000đ đơn con cưng'),
                          Text(
                            '100.000đ tiền thưởng',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '3218 người tham gia',
                            style: TextStyle(color: Colors.orange),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(4.0),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                padding: EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Thử thách của ShopBack',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                    ListTile(
                      leading: const Image(
                        image: AssetImage('assets/img/intvt_logo.jpg'),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text('Mời bạn bất kì thưởng đến 500.00đ'),
                          Text(
                            'Thưởng đến 500.00đ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '5.219 người tham gia',
                            style: TextStyle(color: Colors.orange),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
