import 'package:flutter/material.dart';

class Main_Screen extends StatelessWidget {
  const Main_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final step = [
      'assets/img/img1.PNG',
      'assets/img/img2.PNG',
      'assets/img/img3.PNG',
      'assets/img/img4.PNG',
    ];
    final challenges_titles = [
      'Tặng 50.000đ đơn Coolmate',
      'Mời bạn bất kì thưởng đến 500.000đ',
      'Trúng vocher 4 sao khi đặt ks'
    ];
    final challenges_avt = [
      'assets/img/Coolmate_logo.jpg',
      'assets/img/intvt_logo.jpg',
      'assets/img/vocher.png',
    ];
    final Mall_avt = [
      'assets/img/shopee.jpg',
      'assets/img/lazada.jpg',
      'assets/img/tiki.png',
    ];
    final noibat = [
      'assets/img/beach_icon.png',
      'assets/img/discount_icon.png',
      'assets/img/ivt_icon.PNG',
      'assets/img/game_icon.png',
      'assets/img/chatbot_icon.jpg'
    ];
    final text = ['Vivu', 'Giảm giá', 'Mời bạn', 'Game', 'Chatbot'];
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(scaffoldBackgroundColor: Colors.grey.shade300),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white70,
          leading: Image(
            image: AssetImage('assets/img/sb_logo.png'),
            width: 10,
            height: double.infinity,
            fit: BoxFit.contain,
          ),
          title: Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.grey.shade500,
            ),
            child: Row(
              children: const [
                Icon(Icons.search_outlined),
                Text('Tìm kiếm sản phẩm')
              ],
            ),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () => {},
              icon: const Icon(Icons.notifications_outlined),
              color: Colors.black,
            )
          ],
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(10.0),
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(padding: EdgeInsets.all(8.0)),
              Container(
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: const [
                            Text(
                              'Luân',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text('Tổng số tiền đã hoàn')
                          ],
                        ),
                        const Text(
                          '0 đ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: (() => {
                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text('you click')))
                          }),
                      child: Container(
                        margin: EdgeInsets.only(top: 8.0),
                        padding: const EdgeInsets.all(8),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.blue.shade100),
                        child: Text(
                          'Bắt đầu với nhứng gợi ý tốt nhất từ chúng tôi',
                          style: TextStyle(
                            color: Colors.blue.shade900,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: 150,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: step.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      child: Container(
                        padding: EdgeInsets.all(4.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset(
                              step[index],
                              width: 120,
                              height: 100,
                              fit: BoxFit.fill,
                            )),
                      ),
                      onTap: () => {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text(step[index])))
                      },
                    );
                  },
                ),
              ),
              GestureDetector(
                onTap: (() => {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('you click')))
                    }),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: const Image(
                    image: AssetImage('assets/img/img5.PNG'),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
              ),
              const Text(
                'Thử Thách',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: challenges_titles.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8.0)),
                        child: ListTile(
                          onTap: () {
                            print('object');
                          },
                          title: Text(
                            challenges_titles[index],
                          ),
                          leading: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image(
                              width: 60,
                              height: 80,
                              fit: BoxFit.fill,
                              image: AssetImage(
                                challenges_avt[index],
                              ),
                            ),
                          ),
                          trailing: Container(
                              padding: const EdgeInsets.all(4.0),
                              decoration: BoxDecoration(
                                color: Colors.blue.shade100,
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Text(
                                'Bắt đầu',
                                style: TextStyle(color: Colors.blue.shade900),
                              )),
                        ),
                      ),
                    );
                  },
                ),
              ),
              const Text(
                'Mall Chính Hãng',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              GridView.builder(
                  primary: false,
                  reverse: false,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                  ),
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: (() => {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(Mall_avt[index]),
                              duration: const Duration(microseconds: 20),
                            ))
                          }),
                      child: Container(
                        padding: const EdgeInsets.all(10.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image.asset(
                            Mall_avt[index],
                            width: 100,
                            height: 20,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    );
                  }),
              const Text(
                'Danh mục nổi bật',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              GridView.builder(
                  padding: const EdgeInsets.all(10.0),
                  primary: false,
                  reverse: false,
                  shrinkWrap: true,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5,
                  ),
                  itemCount: noibat.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: (() => {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(noibat[index]),
                              duration: const Duration(microseconds: 20),
                            ))
                          }),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(50.0),
                              child: Image.asset(
                                noibat[index],
                                width: 50,
                                height: 50,
                                fit: BoxFit.cover,
                              ),
                            ),
                            Text(text[index])
                          ],
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
