import 'package:flutter/material.dart';

class Account_Screen extends StatelessWidget {
  const Account_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final icons_setting = [
      Icons.manage_accounts_outlined,
      Icons.vpn_key_outlined,
      Icons.lock_outlined,
      Icons.notifications_outlined,
      Icons.gpp_good_outlined,
      Icons.email_outlined
    ];
    final icon_titles = [
      'Thông tin cá nhâ',
      'Đăng nhập & bảo mật',
      'Cập nhật mật khẩu',
      'Cài Đặt thông báo',
      'Bảo mật',
      'Bản tin ưu đãi mới nhất'
    ];
    return MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: Colors.grey.shade300),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: const Text(
            'Luân',
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Padding(padding: EdgeInsets.all(8.0)),
              Container(
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.all(8.0),
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Số dư khả dụng',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '0 đ',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Text('Tổng sô tiền đã hoàn'),
                    OutlinedButton(
                      child: Text('Rút tiền'),
                      style: OutlinedButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.grey.shade900,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(20))),
                      ),
                      onPressed: () {},
                    ),
                    Text(
                      'Lịch sử hoàn tiền',
                      style:
                          TextStyle(fontSize: 18, color: Colors.blue.shade900),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(4.0),
                decoration: const BoxDecoration(color: Colors.white),
                child: ListTile(
                  onTap: () {},
                  leading: const Icon(
                    Icons.account_circle_outlined,
                  ),
                  title: const Text(
                    'Rủ thêm bạn nhận thên hoàn tiền',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: const Icon(Icons.arrow_forward_ios_outlined),
                ),
              ),
              SizedBox(
                height: 200,
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  padding: const EdgeInsets.all(4.0),
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Column(
                    children: [
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.card_giftcard_outlined,
                        ),
                        title: const Text(
                          'Tiền thưởng',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.emoji_events_outlined,
                        ),
                        title: const Text(
                          'Thử thách',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.done_outline_outlined,
                        ),
                        title: const Text(
                          'Thưởng',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(4.0),
                decoration: const BoxDecoration(color: Colors.white),
                child: ListTile(
                  onTap: () {},
                  leading: const Icon(
                    Icons.favorite_outlined,
                    color: Colors.red,
                  ),
                  title: const Text(
                    'Yêu thích',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: const Icon(Icons.arrow_forward_ios_outlined),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(4.0),
                decoration: const BoxDecoration(color: Colors.white),
                child: ListTile(
                  onTap: () {},
                  leading: const Icon(
                    Icons.star_outlined,
                    color: Colors.yellow,
                  ),
                  title: const Text(
                    'Đánh giá chúng tôi trên Appstore',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  trailing: const Icon(Icons.arrow_forward_ios_outlined),
                ),
              ),
              SizedBox(
                height: 200,
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  padding: const EdgeInsets.all(4.0),
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Column(
                    children: [
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.help_outlined,
                          color: Colors.yellow,
                        ),
                        title: const Text(
                          'Hỗ Trợ',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.help_outlined,
                        ),
                        title: const Text(
                          'Cách hoạt động',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      ListTile(
                        onTap: () {},
                        leading: const Icon(
                          Icons.warning_outlined,
                        ),
                        title: const Text(
                          'Báo lỗi hoàn tiền',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  padding: const EdgeInsets.all(4.0),
                  decoration: const BoxDecoration(color: Colors.white),
                  child: Column(
                    children: [
                      ListTile(
                        leading: Icon(
                          Icons.tune_outlined,
                          color: Colors.blue.shade900,
                        ),
                        title: const Text(
                          'Cài Đặt tài khoản',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: icon_titles.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            leading: Icon(icons_setting[index]),
                            title: Text(icon_titles[index]),
                          );
                        },
                      )
                    ],
                  ),
                ),
              ),
              ButtonTheme(
                minWidth: 100,
                child: OutlinedButton(
                  onPressed: () {},
                  child: Text('Đăng xuất'),
                  style: OutlinedButton.styleFrom(
                      primary: Colors.blue.shade900,
                      backgroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
