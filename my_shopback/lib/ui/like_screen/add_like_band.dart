import 'package:flutter/material.dart';
import 'package:my_shopback/main.dart';
import 'package:my_shopback/ui/like_screen/like_screen.dart';
import 'package:my_shopback/ui/main_screen/main_screen.dart';

class add_band extends StatefulWidget {
  const add_band({Key? key}) : super(key: key);

  @override
  State<add_band> createState() => _add_bandState();
}

class _add_bandState extends State<add_band> {
  @override
  final img = [
    'assets/img/a1.PNG',
    'assets/img/a2.png',
    'assets/img/a3.png',
    'assets/img/a4.png',
    'assets/img/a5.png',
    'assets/img/a6.png',
    'assets/img/a7.png',
    'assets/img/a8.png',
    'assets/img/a9.png',
    'assets/img/a10.png',
  ];

  final band = [
    'Shoppe',
    'Lazada',
    'Tiki',
    'Waston',
    'Booking.com',
    'Agoda',
    'Con Cưng',
    'Nguyễn kim',
    'Hoàng Phúc',
    'ACFC'
  ];
  final discount = [
    'Hoàn đến 6%',
    'Hoàn đến 6%',
    'Hoàn đến 4.7%',
    'Hoàn đến 10%',
    'Hoàn đến 5%',
    'Hoàn đến 6%',
    'Hoàn đến 10%',
    'Hoàn đến 12%',
    'Hoàn đến 3%',
    'Hoàn đến 20%'
  ];
  Color heart = Colors.grey;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Center(
          child: Text(
            "Đối tác",
            style: TextStyle(color: Colors.black),
          ),
        ),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.search_outlined,
                color: Colors.black,
              ))
        ],
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(8.0),
          itemCount: band.length,
          itemBuilder: (context, index) {
            return ListTile(
              onTap: (() => {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(band[index]),
                      duration: const Duration(milliseconds: 20),
                    ))
                  }),
              leading: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  width: 60,
                  height: 80,
                  fit: BoxFit.fill,
                  image: AssetImage(
                    img[index],
                  ),
                ),
              ),
              title: Text(band[index]),
              subtitle: Text(discount[index]),
              trailing: IconButton(
                onPressed: () {
                  setState(() {
                    if (heart == Colors.grey) {
                      heart = Colors.red;
                    } else {
                      heart = Colors.grey;
                    }
                  });
                },
                icon: Icon(
                  Icons.favorite_outlined,
                  color: heart,
                ),
              ),
            );
          }),
    );
  }
}
