import 'package:flutter/material.dart';

import 'package:my_shopback/ui/like_screen/add_like_band.dart';

class Like_Screen extends StatefulWidget {
  const Like_Screen({Key? key}) : super(key: key);

  @override
  State<Like_Screen> createState() => _Like_ScreenState();
}

class _Like_ScreenState extends State<Like_Screen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Yêu Thích',
          style: TextStyle(fontSize: 24, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10.0),
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.fromLTRB(0, 200, 0, 8),
              child: const Center(
                  child: Text(
                'Hiện chưa có gì trong danh sách theo dõi của bạn ',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              )),
            ),
            Container(
              child: const Center(
                  child: Text('Thêm cửa hàng vào danh sách theo dõi của bạn')),
            ),
            OutlinedButton(
              child: Text('Thêm cửa hàng'),
              style: OutlinedButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.grey.shade900,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => add_band()),
              ),
            )
          ],
        ),
      ),
    );
  }
}
