import 'package:flutter/material.dart';

class dt extends StatelessWidget {
  const dt({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dis1 = [
      'assets/img/a1.PNG',
      'assets/img/a2.png',
      'assets/img/a3.png',
      'assets/img/a4.png',
      'assets/img/a5.png',
      'assets/img/a6.png',
      'assets/img/a7.png',
      'assets/img/a8.png',
      'assets/img/a9.png',
      'assets/img/a10.png',
    ];
    return GridView.builder(
        primary: false,
        reverse: false,
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: dis1.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (() {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                duration: Duration(milliseconds: 10),
                content: Text('you click  ${dis1[index]}'),
              ));
            }),
            child: Container(
              child: Image(image: AssetImage(dis1[index])),
            ),
          );
        });
  }
}
