import 'package:flutter/material.dart';

class nb extends StatelessWidget {
  const nb({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final dis2 = [
      'assets/img/b1.png',
      'assets/img/b2.png',
      'assets/img/b3.png',
      'assets/img/b4.png',
      'assets/img/b5.png',
      'assets/img/b6.png',
      'assets/img/b7.png',
    ];
    return GridView.builder(
        primary: false,
        reverse: false,
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: dis2.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (() {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                duration: Duration(milliseconds: 10),
                content: Text('you click  ${dis2[index]}'),
              ));
            }),
            child: Container(
              child: Image(image: AssetImage(dis2[index])),
            ),
          );
        });
  }
}
