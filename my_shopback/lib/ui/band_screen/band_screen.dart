import 'package:flutter/material.dart';

import 'package:my_shopback/ui/band_screen/nb.dart';
import 'package:my_shopback/ui/band_screen/dt.dart';
import 'package:my_shopback/ui/band_screen/tt.dart';

class Band_Screen extends StatelessWidget {
  const Band_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white70,
            title: const Text(
              'Đối Tác',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.black),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () => {},
                icon: const Icon(Icons.search_outlined),
                color: Colors.black,
              )
            ],
            bottom: const TabBar(tabs: [
              Tab(
                child: Text(
                  'Nổi bật',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Tab(
                child: Text(
                  'Điện tử và gia dụng',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Tab(
                child: Text(
                  'Thời trang',
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ]),
          ),
          body: const TabBarView(children: [dt(), nb(), tt()]),
        ),
      ),
    );
  }
}
