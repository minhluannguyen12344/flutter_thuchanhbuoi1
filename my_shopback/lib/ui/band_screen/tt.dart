import 'package:flutter/material.dart';

class tt extends StatefulWidget {
  const tt({Key? key}) : super(key: key);

  @override
  State<tt> createState() => _ttState();
}

class _ttState extends State<tt> {
  @override
  Widget build(BuildContext context) {
    final dis3 = [
      'assets/img/c3.png',
      'assets/img/c4.png',
      'assets/img/c5.png',
      'assets/img/c6.png',
      'assets/img/c7.PNG',
      'assets/img/c8.png',
      'assets/img/c9.png',
      'assets/img/c10.png',
      'assets/img/c11.png',
      'assets/img/c12.png',
    ];
    return GridView.builder(
        primary: false,
        reverse: false,
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: dis3.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: (() {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                duration: Duration(milliseconds: 10),
                content: Text('you click  ${dis3[index]}'),
              ));
            }),
            child: Container(
              child: Image(image: AssetImage(dis3[index])),
            ),
          );
        });
  }
}
