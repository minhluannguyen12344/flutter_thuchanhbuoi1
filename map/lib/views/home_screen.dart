import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Homescreen extends StatefulWidget {
  const Homescreen({Key? key}) : super(key: key);

  @override
  State<Homescreen> createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  late LatLng userPosition;
  List<Marker> markers = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Google Maps'),
      ),
      body: FutureBuilder(
          future: findUserLocation(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return GoogleMap(
              markers: Set<Marker>.of(markers),
              initialCameraPosition: CameraPosition(
                target: LatLng(51.5285582, -0.24167),
              ),
            );
          }),
    );
  }

  Future<LatLng> findUserLocation() async {
    Location location = Location();
    LocationData userLocation;
    PermissionStatus hasPermission = await location.hasPermission();
    bool active = await location.serviceEnabled();
    if (hasPermission == PermissionStatus.granted && active) {
      userLocation = await location.getLocation();

      userPosition = LatLng(
          userLocation.latitude as double, userLocation.longitude as double);
    } else {
      print('Use default location...');
      // TDT University
      userPosition = LatLng(10.732869174213993, 106.69973741130023);
    }

    // Display marker at the user position
    if (markers.isEmpty) {
      markers.add(buildMaker(userPosition));
    } else {
      markers[0] = buildMaker(userPosition);
    }
    setState(() {});

    return userPosition;
  }

  Marker buildMaker(LatLng pos) {
    MarkerId markerId = MarkerId('H');
    Marker marker = Marker(markerId: markerId, position: pos);

    return marker;
  }
}
