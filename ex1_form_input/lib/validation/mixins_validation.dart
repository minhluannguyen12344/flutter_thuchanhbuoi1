mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains(RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+"))) {
      return 'Pls input valid email.';
    }
    return null;
  }

  String? valdiateHoTenLot(String? value) {
    if (value == null || value.isEmpty) {
      return 'Pls nhập họ và tên lót.';
    }
    return null;
  }

  String? valdiateTen(String? value) {
    if (value == null || value.isEmpty) {
      return 'Pls nhập Tên.';
    }
    return null;
  }

  String? valdiateYear(String? value) {
    if (!value!.contains(RegExp(r'^[12][0-9]{3}$'))) {
      return 'Pls nhập năm sinh hợp lệ';
    }
    return null;
  }
}
