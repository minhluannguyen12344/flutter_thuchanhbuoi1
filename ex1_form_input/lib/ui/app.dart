import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../validation/mixins_validation.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "student",
      home: Scaffold(
        appBar: AppBar(title: Text('Student information')),
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }
}

class LoginState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String HoTenLot;
  late String Ten;
  late String Year;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
      key: formKey,
      child: Column(
        children: [
          fieldEmailAddress(),
          Container(
            margin: EdgeInsets.only(top: 20.0),
          ),
          fieldHoTenLot(),
          Container(
            margin: EdgeInsets.only(top: 20.0),
          ),
          fieldTen(),
          Container(
            margin: EdgeInsets.only(top: 20.0),
          ),
          fieldYear(),
          Container(
            margin: EdgeInsets.only(top: 20.0),
          ),
          loginButton()
        ],
      ),
    ));
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration:
          InputDecoration(icon: Icon(Icons.person), labelText: 'Email address'),
      validator: validateEmail,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldHoTenLot() {
    return TextFormField(
      decoration:
          InputDecoration(icon: Icon(Icons.edit), labelText: 'Họ và tên lót'),
      validator: valdiateHoTenLot,
      onSaved: (value) {
        HoTenLot = value as String;
      },
    );
  }

  Widget fieldTen() {
    return TextFormField(
      decoration:
          InputDecoration(icon: Icon(Icons.edit), labelText: 'Nhập Tên'),
      validator: valdiateTen,
      onSaved: (value) {
        Ten = value as String;
      },
    );
  }

  Widget fieldYear() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.calendar_today), labelText: 'Nhập năm sinh'),
      validator: valdiateYear,
      onSaved: (value) {
        Year = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress,$HoTenLot,$Ten,$Year');
          }
        },
        child: Text('Enter'));
  }
}
